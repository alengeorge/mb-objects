function invert(obj) {

  if(typeof obj !== 'object'|| Array.isArray(obj) || !obj){
    return "Invalid Input"
  }
  let invertedObj = {};

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (!invertedObj.hasOwnProperty(obj[key])) {
        invertedObj[obj[key]] = key;
      } else {
        return "Duplicate Key Found";
      }
    }
  }

  return invertedObj;
}

module.exports = invert;
