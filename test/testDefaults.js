let obj = require("../objects");

let getDefaults = require("../defaults");

let defaultProps = { name: "Alfred", age: 50, AKA: "The BatMan" };

console.log(getDefaults(obj, defaultProps));
