function values(obj) {

    let valuesArray = []

    for(let key in obj){
        if (obj.hasOwnProperty(key)) {
        valuesArray.push(obj[key])
        }
    }

return valuesArray
}

module.exports = values