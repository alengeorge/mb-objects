function keys(obj) {
  let objKeys = [];

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      objKeys.push(key);
    }
  }

  return objKeys;
}
module.exports = keys;
