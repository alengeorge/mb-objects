function mapObject(obj, cb) {
  let newObj = {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      newObj[key]=cb(obj[key]);
    }
  }

  return newObj;
}

module.exports = mapObject;
