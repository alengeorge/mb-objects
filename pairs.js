
function pairs(obj) {
  let pairsArr = [];

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      pairsArr.push([key, obj[key]]);
    }
  }

  return pairsArr
}

module.exports = pairs;
